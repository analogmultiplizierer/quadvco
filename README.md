# quadLFO
a current controllable Low Frequency Oscillator module largely derived from http://yusynth.net/Modular/EN/QUAD-LFO/index.html

The Board Layout has one signal layer only to aid etching/milling.
The rear layer is used for negative supply voltage exclusively, only a few connections need to be made

![](./3D.png)

## usage

Two equal capacitors need to be connected to J1-1/J1-2 and J1-3/J1-4.
The capacitor value sets the usable frequency range
~10F-100nF provide a useful NF-Range
Connect +15V to J2-1, -15V to J2-4 and GND to J2-3
J2-2 is a virtual ground. A current source must be connected to set the LFO's frequnecy.
You may connect a Resistor to obtain voltage control. You may connect a second resistor to set a minimum frequency.

The Sine/Cosine Outputs are connected to J1-1 and J1-3

## operation

In the circuit one LM13700 OTA and one TL082 OpAmp form two integrators with current controllable time constants.
The integrators are configured in series to provide 90° and 180° phase shift.
A half TL082 OpAmp forms an Inverting Amplifier with nonlinear feedback to limit the amplitude of the amplifier and to invert (180° shift) the signal.
The output signal (360° shifted in total) is fed back to the first integrator to close the oscillators loop
Another half TL082 OpAmp controls the I<sub>ABC</sub> current for both LM13700 halves, setting the integrators (equal) time constants and thus the resulting frequency
